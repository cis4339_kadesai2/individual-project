class CreateSuppliers < ActiveRecord::Migration
  def change
    create_table :suppliers do |t|
      t.integer :product_id
      t.date :date_r
      t.integer :qty_r

      t.timestamps
    end
  end
end
