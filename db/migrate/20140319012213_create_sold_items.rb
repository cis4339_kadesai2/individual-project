class CreateSoldItems < ActiveRecord::Migration
  def change
    create_table :sold_items do |t|
      t.integer :sale_id
      t.integer :product_id
      t.date :start_date
      t.date :end_date

      t.timestamps
    end
  end
end
