class Supplier < ActiveRecord::Base
  belongs_to :product

  validates_presence_of :product_id, :date_r, :qty_r
  validates_numericality_of :qty_r, :greater_than_or_equal_to => 1, :only_integer => true

  after_create :update_product_qty

  def update_product_qty
    self.product.update_attribute("qty", (product.qty + self.qty_r))
  end

end
