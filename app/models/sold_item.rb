class SoldItem < ActiveRecord::Base
  belongs_to :product
  belongs_to :sale
  accepts_nested_attributes_for :product, :sale

  def sales
    @sales ||= find_sales
  end

  private

  def find_sales
    sales = sales.where("date >= ?", start_date)
    sales = sales.where("date <= ?", end_date)
    sales
  end



end
