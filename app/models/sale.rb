class Sale < ActiveRecord::Base

  validates_presence_of :product_id, :date, :price, :qty
  validates_numericality_of :qty, :greater_than_or_equal_to => 1, :only_integer => true
  validates_numericality_of :qty, :less_than_or_equal_to => Proc.new {|sale|sale.product.qty}, :message => "Not enough quantity in stock, please modify selected quantity"
  validates_numericality_of :price, :greater_than_or_equal_to => 0.01


  belongs_to :product
  has_many :searches
  accepts_nested_attributes_for :product

  before_create :calculate_total

  def calculate_total
    self.total = self.qty * self.price
  end

  after_create :reduce_product_qty

  def reduce_product_qty
    self.product.update_attribute("qty", (product.qty - self.qty))
  end

  def self.search(query)
    where("sale_id >= ?", "%#{query}%")
  end
end
