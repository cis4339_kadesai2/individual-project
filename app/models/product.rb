class Product < ActiveRecord::Base
  has_many :sales
  has_many :searches
  has_many :suppliers

  validates_presence_of :name, :qty
  validates_uniqueness_of :name
  validates_numericality_of :qty, :greater_than_or_equal_to => 0, :only_integer => true

  def self.search_min_quantity(query)
    where("qty <= ?",  "#{query}")
  end
end
