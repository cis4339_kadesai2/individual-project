json.array!(@sold_items) do |sold_item|
  json.extract! sold_item, :id, :sale_id, :product_id, :start_date, :end_date
  json.url sold_item_url(sold_item, format: :json)
end
