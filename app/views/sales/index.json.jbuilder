json.array!(@sales) do |sale|
  json.extract! sale, :id, :product_id, :date, :qty, :price, :total
  json.url sale_url(sale, format: :json)
end
