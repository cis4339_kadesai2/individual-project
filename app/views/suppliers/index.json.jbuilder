json.array!(@suppliers) do |supplier|
  json.extract! supplier, :id, :product_id, :date_r, :qty_r
  json.url supplier_url(supplier, format: :json)
end
